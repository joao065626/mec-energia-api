import pytest
from users.models import CustomUser
from utils.user.authentication import generate_random_password
from django.conf import settings

@pytest.fixture
def user_model():
    return CustomUser

@pytest.mark.django_db
class TestUserCreation:
    def test_email_required(self, user_model):
        with pytest.raises(ValueError, match="Email is required"):
            user_model.create(email=None)

    def test_valid_user_creation(self, user_model, mocker):
        mocker.patch.object(Password, 'send_email_first_access_password')  # Mocking the send_email_first_access_password method

        email = "joao091435@gmail.com"
        user = user_model.create(email=email, name="Joao")

        assert user.email == email
        assert user.type  # Assuming user.type is required and is set properly
        assert user.check_password("dummy_password")  # Assuming a password is set properly

        # Mocked method should be called in this case
        Password.send_email_first_access_password.assert_called_once_with(user)

    def test_invalid_user_type(self, user_model):
        with pytest.raises(Exception, match="Error Create User"):
            user_model.create(email="joao091435@gmail.com", type="342212")

    def test_password_required(self, user_model, mocker):
        mocker.patch.object(Password, 'send_email_first_access_password')  # Mocking the send_email_first_access_password method

        email = "tester"
        with pytest.raises(Exception, match="User not identified, password is required"):
            user_model.create(email=email)

        # Mocked method should not be called in this case
        Password.send_email_first_access_password.assert_not_called()

    def test_environment_check(self, user_model, mocker):
        mocker.patch.object(Password, 'send_email_first_access_password')  # Mocking the send_email_first_access_password method

        email = "tester"
        with pytest.raises(Exception, match="User not identified, password is required"):
            user_model.create(email=email)

        # Mocked method should not be called in this case
        Password.send_email_first_access_password.assert_not_called()

    def test_university_user_creation(self, user_model, mocker):
        mocker.patch.object(Password, 'send_email_first_access_password')  # Mocking the send_email_first_access_password method

        email = "university_user"
        user = user_model.create(email=email)

        assert user.type  # Assuming user.type is required and is set properly
        assert user.check_password("dummy_password")  # Assuming a password is set properly

        # Mocked method should be called in this case
        Password.send_email_first_access_password.assert_called_once_with(user)

    def test_invalid_university_user_type(self, user_model):
        with pytest.raises(Exception, match="Error Create User"):
            user_model.create(email="universo")